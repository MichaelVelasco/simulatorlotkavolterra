export class GlobalConstants {
  public static window: any = window;
  public static mathJax:any = GlobalConstants.window.MathJax;
  public static basicEquation = '$$ \\left\\{ \\begin{array}{c} x\'(t)=ax+bxy \\\\ y\'(t)=-cx+dxy \\end{array} \\right. $$';
}
