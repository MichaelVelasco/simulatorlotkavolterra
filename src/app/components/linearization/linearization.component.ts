import { Component, ElementRef, NgModuleFactoryLoader, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { cos, sin, sqrt, unit } from 'mathjs';
import { GlobalConstants } from 'src/app/common/global-constants';

declare function startChart(): any;

declare var graph:any;

@Component({
  selector: 'app-linearization',
  templateUrl: './linearization.component.html',
  styleUrls: ['./linearization.component.css']
})
export class LinearizationComponent implements OnInit {

  @ViewChild('generalEcuation') generalEcuation!:ElementRef;
  @ViewChild('balancePoints') balancePoints!:ElementRef;
  @ViewChild('JacobianMatrix') jacobianMatrix!:ElementRef;
  @ViewChild('JacobianMatrixSolved') jacobianMatrixSolved!:ElementRef;
  @ViewChild('JacobianMatrixSolvedValues') jacobianMatrixSolvedValues!:ElementRef;
  @ViewChild('eigenValues') eigenValues!:ElementRef;
  @ViewChild('eigenVectors') eigenVectors!:ElementRef;
  @ViewChild('finalEquation') finalEquation!:ElementRef;
  @ViewChild('constantsStep1') constantsStep1!:ElementRef;
  @ViewChild('constantsStep2') constantsStep2!:ElementRef;
  @ViewChild('constantsStep3') constantsStep3!:ElementRef;
  @ViewChild('constantsStep4') constantsStep4!:ElementRef;

  a:number=0.4;
  b:number=0.018;
  c:number=0.8;
  d:number=0.023;

  xInit:number=30;
  yInit:number=4;

  time:number = 0;

  degRad:string = 'rad';

  step:number = 2;

  cycle:boolean = false;
  speedInSeconds:number = 0.1;

  moreStep:boolean = true;
  prevStep:boolean = false;

  allSteps:boolean = false;
  allButtonText:string = 'Todo';

  constructor() {
  }

  ngOnInit(): void {
    try {
      this.render();
      this.loadChart();
    } catch (error) {
      setTimeout(()=>{
        this.ngOnInit();
      })
    }
  }

  loadChart():void{
    graph.init('graph');
    graph.initGraph(this.getDataToChart());
  }

  getDataToChart():any{
    var ret:object[] = [];
    for (let i = this.time; i <= (this.time+20); i=i+1) {
      ret.push({
        x:i,
        y:this.xOft(i),
        z:this.yOft(i),
      });
    }
    return ret;
  }

  changeTime(event:any):void{
    this.time = Math.round(this.time);
    this.loadChartData();
  }
  changeSpeed(event:any):void{
    this.stopCycle();
    if(this.cycle){
      this.startCycle();
    }
  }
  cycleEvent(event:any):void{
    this.cycle = event.target.checked;
    if(this.cycle){
      this.startCycle();
    }else{
      this.stopCycle();
    }
  }

  cycleInterval:any;

  startCycle():void{
    this.cycleInterval = setInterval(()=>{
      this.time++;
      this.loadChartData();
    }, this.speedInSeconds*1000)
  }
  stopCycle():void{
    clearInterval(this.cycleInterval);
  }

  degreeRad(event:any):void{
    this.degRad = event.target.value;
    this.loadChartData();
  }

  loadChartData():void{
    graph.setData(this.getDataToChart());
  }

  change(event: any):void{
    var valid:boolean = this.betwenZeroToOne(parseInt(event.target.value));
    if(event.target.id == 'a'){
      this.a = valid ? this.a : (parseInt(event.target.value)>=0.5 ? 1 : 0.1);
    }
    if(event.target.id == 'b'){
      this.b = valid ? this.b : (parseInt(event.target.value)>=0.5 ? 1 : 0.1);
    }
    if(event.target.id == 'c'){
      this.c = valid ? this.c : (parseInt(event.target.value)>=0.5 ? 1 : 0.1);
    }
    if(event.target.id == 'd'){
      this.d = valid ? this.d : (parseInt(event.target.value)>=0.5 ? 1 : 0.1);
    }
    this.render();
    this.loadChartData();
  }

  //validaciones generales

  betwenZeroToOne(value:number):boolean {
    return value <= 1 && value >= 0;
  }

  //textos generales

  get ec1():string{
    return this.a+'x-'+this.b+'xy';
  }
  get ec2():string{
    return '-'+this.c+'y+'+this.d+'xy';
  }
  get valVector():string{
    return 'i\\frac{'+(this.b*sqrt(this.c))+'}{'+(sqrt(this.a)*this.d)+'}';
  }
  get aValue():number{
    return ((this.d/this.c)*(sqrt(this.a/ this.b)));
  }

  xOft(t:number):number{
    var res = ( this.xInit * cos(unit(this.eigenvalues()*t, this.degRad)) ) + ( (this.yInit/(-1*this.aValue)) * sin(unit(this.eigenvalues()*t, this.degRad)) );
    // console.log('x('+t+')= ('+this.xInit+'*cos('+this.eigenvalues()+'*'+t+'))+'+'('+(this.yInit/(-1*this.aValue)+'*sin('+this.eigenvalues()+'*'+t+')) = '+res));
    return res;
  }
  yOft(t:number):number{
    var res = this.round((this.xInit * this.aValue * sin(unit(this.eigenvalues()*t, this.degRad)) ) + ( (this.yInit/(-1*this.aValue)) * (-1*this.aValue) * cos(unit(this.eigenvalues()*t, this.degRad))));
    // console.log('y('+t+')= (('+this.xInit +'*'+ this.aValue +' * sin('+this.eigenvalues()+'*'+t+'))+(('+ (this.yInit/(-1*this.aValue)) +')*('+ (-1*this.aValue) +')*(cos('+this.eigenvalues()+'*'+t+')) = '+res);
    return res;
  }
  // Sobre Matriz jacobiana

  point_1_x():number{
    return this.round(this.c/this.d);
  }
  point_1_y():number{
    return this.round(this.a/this.b);
  }

  //paso a paso

  nextStep():void{
    this.step = this.step+2;
    this.prevStep=true;
    this.removeAll();

    if(this.step == 4){
      this.jacobianMatrix.nativeElement.classList.add('active');
      this.jacobianMatrixSolved.nativeElement.classList.add('active');
    }else if(this.step == 6){
      this.jacobianMatrixSolvedValues.nativeElement.classList.add('active');
      this.eigenValues.nativeElement.classList.add('active');
      this.eigenVectors.nativeElement.classList.add('active');
    }else if(this.step == 8){
      this.finalEquation.nativeElement.classList.add('active');
    }else if(this.step == 10){
      this.constantsStep1.nativeElement.classList.add('active');
      this.constantsStep2.nativeElement.classList.add('active');
      this.constantsStep3.nativeElement.classList.add('active');
    }else if(this.step == 12){
      this.constantsStep4.nativeElement.classList.add('active');
      this.moreStep=false;
    }else{
      this.step = 10;
      this.moreStep=false;
    }
  }
  backStep():void{
    this.step = this.step-2;
    this.moreStep=true;
    this.removeAll();

    if(this.step == 10){
      this.constantsStep1.nativeElement.classList.add('active');
      this.constantsStep2.nativeElement.classList.add('active');
      this.constantsStep3.nativeElement.classList.add('active');
    }else if(this.step == 8){
      this.finalEquation.nativeElement.classList.add('active');
    }else if(this.step == 6){
      this.jacobianMatrixSolvedValues.nativeElement.classList.add('active');
      this.eigenValues.nativeElement.classList.add('active');
      this.eigenVectors.nativeElement.classList.add('active');
    }else if(this.step == 4){
      this.jacobianMatrix.nativeElement.classList.add('active');
      this.jacobianMatrixSolved.nativeElement.classList.add('active');
    }else if(this.step == 2){
      this.generalEcuation.nativeElement.classList.add('active');
      this.balancePoints.nativeElement.classList.add('active');
      this.prevStep = false;
    }else{
      this.step = 2;
      this.moreStep = true;
    }
  }
  removeAll():void{
    this.generalEcuation.nativeElement.classList.remove('active');
    this.balancePoints.nativeElement.classList.remove('active');
    this.jacobianMatrix.nativeElement.classList.remove('active');
    this.jacobianMatrixSolved.nativeElement.classList.remove('active');
    this.jacobianMatrixSolvedValues.nativeElement.classList.remove('active');
    this.eigenValues.nativeElement.classList.remove('active');
    this.eigenVectors.nativeElement.classList.remove('active');
    this.finalEquation.nativeElement.classList.remove('active');
    this.constantsStep1.nativeElement.classList.remove('active');
    this.constantsStep2.nativeElement.classList.remove('active');
    this.constantsStep3.nativeElement.classList.remove('active');
    this.constantsStep4.nativeElement.classList.remove('active');
  }
  allStep():void{
    if(this.allSteps){
      this.allButtonText = 'Todo';
      this.generalEcuation.nativeElement.classList.remove('active-steps');
      this.balancePoints.nativeElement.classList.remove('active-steps');
      this.jacobianMatrix.nativeElement.classList.remove('active-steps');
      this.jacobianMatrixSolved.nativeElement.classList.remove('active-steps');
      this.jacobianMatrixSolvedValues.nativeElement.classList.remove('active-steps');
      this.eigenValues.nativeElement.classList.remove('active-steps');
      this.eigenVectors.nativeElement.classList.remove('active-steps');
      this.finalEquation.nativeElement.classList.remove('active-steps');
      this.constantsStep1.nativeElement.classList.remove('active-steps');
      this.constantsStep2.nativeElement.classList.remove('active-steps');
      this.constantsStep3.nativeElement.classList.remove('active-steps');
      this.constantsStep4.nativeElement.classList.remove('active-steps');
    }else{
      this.allButtonText = 'Por Pasos';
      this.generalEcuation.nativeElement.classList.add('active-steps');
      this.balancePoints.nativeElement.classList.add('active-steps');
      this.jacobianMatrix.nativeElement.classList.add('active-steps');
      this.jacobianMatrixSolved.nativeElement.classList.add('active-steps');
      this.jacobianMatrixSolvedValues.nativeElement.classList.add('active-steps');
      this.eigenValues.nativeElement.classList.add('active-steps');
      this.eigenVectors.nativeElement.classList.add('active-steps');
      this.finalEquation.nativeElement.classList.add('active-steps');
      this.constantsStep1.nativeElement.classList.add('active-steps');
      this.constantsStep2.nativeElement.classList.add('active-steps');
      this.constantsStep3.nativeElement.classList.add('active-steps');
      this.constantsStep4.nativeElement.classList.add('active-steps');
    }
    this.allSteps = !this.allSteps;
  }

  //functiones generales

  eigenvalues():number{
    return sqrt(this.a)*sqrt(this.c);
  }

  round(number:number):number{
    var decimals = number - Math.floor(number);
    var decimalS:string = decimals.toString();
    var res = decimalS.substr(2, 3);
    if(this.allEqual(res)){
      if(res.split('')[0] == '9'){
        return (number-decimals)+Math.round(decimals);
      }else if(res.split('')[0] == '0'){
        return (number-decimals);
      }
    }
    return number;
  }

  allEqual(input:string):boolean {
    return input.split('').every(char => char === input[0]);
  }

  render():void{
    this.generalEcuation.nativeElement.innerHTML = '$$ \\left\\{ \\begin{array}{c} x\'(t)='+this.ec1+' \\\\ y\'(t)='+this.ec2+' \\end{array} \\right. $$ ';

    this.balancePoints.nativeElement.innerHTML = '$$ \\text{ Puntos de Equilibrio } $$ $$ \\left\\{ \\begin{array}{c} '+this.ec1+'=0 \\\\ '+this.ec2+'=0 \\end{array} \\right. $$ $$ p_0=(0,0) $$ $$  p_1=('+this.point_1_x()+','+this.point_1_y()+') $$';

    this.jacobianMatrix.nativeElement.innerHTML = '$$ \\text{ Matriz Jacobiana } $$ $$ J = \\begin{pmatrix} \\frac{d}{dx}\\left('+this.ec1+'\\right)\\  & \\frac{d}{dy}\\left('+this.ec1+'\\right)\\ \\\\ \\frac{d}{dx}\\left('+this.ec2+'\\right)\\ & \\frac{d}{dy}\\left('+this.ec2+'\\right)\\ \\end{pmatrix} $$';

    this.jacobianMatrixSolved.nativeElement.innerHTML = '$$ J = \\begin{pmatrix} '+this.a+'-'+this.b+'y\\  & -'+this.b+'x \\\\ -'+this.d+'y & -'+this.c+'+'+this.d+'x \\end{pmatrix} $$';

    this.jacobianMatrixSolvedValues.nativeElement.innerHTML = '$$ \\text{ Valores y Vectores Propios } $$ $$ J\\lvert p_1 \\lvert = \\begin{pmatrix} '+(this.a-(this.b*this.point_1_y()))+'\\  & '+(this.b*this.point_1_x()*-1)+' \\\\ '+(-1*(this.d*this.point_1_y()))+' & '+((-1*this.c)+(this.d*this.point_1_x()))+' \\end{pmatrix} $$';

    this.eigenValues.nativeElement.innerHTML = '$$ \\lambda_{1-2} = ±i '+this.eigenvalues()+' $$ ';

    this.eigenVectors.nativeElement.innerHTML = '$$ K_{1} = \\begin{bmatrix} -'+this.valVector+' \\\\ 1 \\end{bmatrix}  K_{2} = \\begin{bmatrix} '+this.valVector+' \\\\ 1 \\end{bmatrix}$$ ';

    this.finalEquation.nativeElement.innerHTML = '$$ \\text{ Solución General } $$ $$ \\left\\{ \\begin{array}{c} x(t)=c_1 cos('+this.eigenvalues()+'t)+c_2sen('+this.eigenvalues()+'t) \\\\ y(t)= c_1('+this.aValue+')sen('+this.eigenvalues()+'t) + c_2('+(-1*this.aValue)+'t)cos('+this.eigenvalues()+'t) \\end{array} \\right. $$ ';

    this.constantsStep1.nativeElement.innerHTML = '$$ \\left\\{ \\begin{array}{c} '+this.xInit+'=c_1 cos('+this.eigenvalues()+'*0)+c_2sen('+this.eigenvalues()+'*0) \\\\ '+this.yInit+'= c_1('+this.aValue+')sen('+this.eigenvalues()+'*0) + c_2('+(-1*this.aValue)+')cos('+this.eigenvalues()+'*0) \\end{array} \\right. $$ ';

    this.constantsStep2.nativeElement.innerHTML = '$$ \\left\\{ \\begin{array}{c} '+this.xInit+'=c_1 cos(0)+c_2sen(0) \\\\ '+this.yInit+'= c_1('+this.aValue+')sen(0) + c_2('+(-1*this.aValue)+')cos(0) \\end{array} \\right. $$ ';

    this.constantsStep3.nativeElement.innerHTML = '$$ \\left\\{ \\begin{array}{c} c_1='+this.xInit+' \\\\ c_2 = '+(this.yInit/(-1*this.aValue))+' \\end{array} \\right. $$ ';

    this.constantsStep4.nativeElement.innerHTML = '$$ \\text{ Solución Específica} $$ $$ \\left\\{ \\begin{array}{c} x(t)=('+this.xInit+') cos('+this.eigenvalues()+'t)+('+(this.yInit/(-1*this.aValue))+')sen('+this.eigenvalues()+'t) \\\\ y(t)= ('+this.xInit+')('+this.aValue+')sen('+this.eigenvalues()+'t) + ('+(this.yInit/(-1*this.aValue))+')('+(-1*this.aValue)+')cos('+this.eigenvalues()+'t) \\end{array} \\right. $$ ';

    GlobalConstants.window.MathJax.typesetPromise();
  }
}
