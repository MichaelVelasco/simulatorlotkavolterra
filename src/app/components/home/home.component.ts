import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalConstants } from 'src/app/common/global-constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css', '../../app.component.css']
})
export class HomeComponent implements OnInit {

  lisModels: any[] =  [
    {
      name: 'Linealización',
      img:'/assets/images/image1.jpg',
      route: 'linearization',
      active: true
    },
    {
      name: 'Modelo Base',
      img:'/assets/images/image2.jpg',
      route: '',
      active: false
    },
    {
      name: 'Modelo por competencia',
      img:'/assets/images/image3.jpg',
      route: '',
      active: false
    },
  ]

  constructor(private router: Router) {

  }

  ngOnInit(): void {
  }

  loadModel(route:string): void{
    this.router.navigate([route]);
  }

}
