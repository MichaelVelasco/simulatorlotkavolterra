class Graph{

  repets = 0;

  init(idContainer){
    this.idContainer = idContainer;
  }

  initGraph(initData){
    var that = this;
    this.graph =  Morris.Line({
      element: this.idContainer,
      data: initData,
      xkey: 'x',
      ykeys: ['y', 'z'],
      labels: ['x(t)', 'y(t)'],
      parseTime: false,
      ymin: 'auto',
      ymax: 'auto',
      xmin: 'auto',
      xmax: 'auto',
      hideHover: true
    });

    // setInterval(() => {
    //   that.repets++
    //   that.setData(this.data(this.repets));
    // }, 100);
  }

  data(offset) {
    var ret = [];
    for (var x = 0; x <= 180; x += 10) {
      var v = (offset + x) % 360;
      ret.push({
        x: x,
        y: Math.sin(Math.PI * v / 180).toFixed(4),
        z: Math.cos(Math.PI * v / 180).toFixed(4)
      });
    }
    return ret;
  }

  setData(data){
    this.graph.setData(data);
  }

  changeYmin(ymin){
    this.graph.ymin = ymin;
  }

  reDraw(){
    this.graph.redraw();
  }

}
